![alt text](img/tc.jpg "Tc")<br/>
# Быстрая установка TeamCity в докер контейнере

## Настройка сервера TeamCity

### Ставим Docker    
    apt update
    apt install apt-transport-https ca-certificates curl software-properties-common
    curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
    add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu bionic stable"
    apt update
    apt-cache policy docker-ce
    apt install docker-ce
    systemctl status docker

### Стартуем TeamCity Server в докер контейнере
    docker run \
    -v team_city_data:/data/teamcity_server/datadir \
    -v team_city_logs:/opt/teamcity/logs  \
    -p 8111:8111 \
	-d jetbrains/teamcity-server
Здесь переходим по адресу http://адрес_вашей_машины_тимсити_сервера:8111 и проходим нехитрую процедуру настройки TeamCity-сервера.

## Настройка TeamCity агента

### Ставим Docker
    apt update
    apt install apt-transport-https ca-certificates curl software-properties-common
    curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
    add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu bionic stable"
    apt update
    apt-cache policy docker-ce
    apt install docker-ce
    systemctl status docker

### Чтобы монтирование докера работало, а команды докера могли выполняться внутри контейнера с агентом TeamCity, необходимо установить менее строгие разрешения для докера на хосте где ставим TeamCity агента с помощью следующей команды:
    chmod 666 /var/run/docker.sock

### Стартуем TeamCity Agent в докер контейнере
    docker run -e SERVER_URL="http://public-ip:8111"  \
    -v team_city_agent_config_two:/data/teamcity_agent/conf  \
    -v /var/run/docker.sock:/var/run/docker.sock \
    -d jetbrains/teamcity-agent
Идем снова на http://адрес_вашей_машины_тимсити_сервера:8111 переходим в список агентов и авторизуем новый TeamCity Agent.