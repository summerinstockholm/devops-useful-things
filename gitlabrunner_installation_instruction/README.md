![alt text](img/gitlabrunner.jpg "GitLab")<br/>
# Настройка-настроечка GitLab проджект раннера.
## Итак, есть хост на котором имеется желание собирать Docker-образы или допустим эти Docker-образы деплоить (например через shell).
### Проджект раннеры смотрим в gitlab.com/название_проекта/-/settings/ci_cd. Идем на это хост и ставим сначала докер:
`sudo apt update`<br/>
`sudo apt install apt-transport-https ca-certificates curl software-properties-common`<br/>
`curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -`<br/>
`sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu bionic stable"`<br/>
`sudo apt update`<br/>
`apt-cache policy docker-ce`<br/>
`sudo apt install docker-ce`<br/>
#### гитлаб раннер (используя официальный гитлаб репозиторий):<br/>
`curl -L "https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh" | sudo bash`<br/>
`sudo apt-get install gitlab-runner`<br/>
#### а затем зарегистрировать проджект раннер (в качестве экзекьютора - докер):<br/>
`sudo gitlab-runner register`<br/>
`Enter the GitLab instance URL`<br/>
`https://gitlab.com`<br/>
`Enter the registration token:`<br/>
#### вводить токен из /название_проекта/-/settings/ci_cd<br/>
`Enter a description for the runner:`<br/>
#### пишем например ya-docker-vm<br/>
`Enter tags for the runner (comma-separated):`<br/>
#### пишем docker<br/>
`Enter optional maintenance note for the runner:`<br/>
#### ничего не пишем<br/>
`Enter an executor:`<br/>
#### пишем docker<br/>
`Enter the default Docker image:`<br/>
#### пишем базовый докер образ - python:3-alpine<br/>
### На этом раннер для докера зарегестрирован, получается.<br/>
### Регистрируем проджект раннер в качестве экзекьютора которого будет shell:
`sudo gitlab-runner register`<br/>
`Enter the GitLab instance URL`<br/>
`https://gitlab.com`<br/>
`Enter the registration token:`<br/>
#### вводить токен из /название_проекта/-/settings/ci_cd<br/>
`Enter a description for the runner:`<br/>
#### пишем например ya-shell-vm<br/>
`Enter tags for the runner (comma-separated):`<br/>
#### пишем shell<br/>
`Enter optional maintenance note for the runner:`<br/>
#### ничего не пишем<br/>
`Enter an executor:`<br/>
#### пишем shell<br/>
### В /название_проекта/-/settings/ci_cd в проджект раннерах должны появиться два раннера.<br/>
### В группу докер добавить пользователя gitlab-runner.<br/>
`sudo adduser gitlab-runner docker`<br/>