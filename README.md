![alt text](img/human.jpg "Human")<br/>
# Здесь будут полезные вещи (команды, инструкции, ссылки) по девопс вопросам.

[Быстрая установка TeamCity в докер контейнере](../teamcity_installation_instruction/README.md)

[Настройка-настроечка собственного GitLab-runner](../gitlabrunner_installation_instruction/README.md)

[Как поднять кубер и не сойти с ума](../kubernetes_installation_instruction/README.md)