![alt text](img/kuber.jpg "Tc")<br/>
# Как поднять свой кластер Kubernetes и не сойти с ума.
Сразу оговорюсь, что вероятно это не best-practice и многое так как сделано здесь делать в реальности не нужно, но имеем то что имеем.<br/>
Особенно плохо когда кластер торчит наружу в интернет, но для тренировки пойдет и такой вариант.<br/>

## Подготовим две машины. На одной будет control-plane и master, на второй worker.
### Решаем вопросы со свапом
swapoff -a<br/>
sed -i '/swap/ s/^/#/' /etc/fstab
### Отключаем файерволл
systemctl stop ufw<br/>
systemctl disable ufw
### Задаем hostname
hostname set-hostname ***хостнеймкакойхотим***<br/>
sytmectl restart systemd-hostnamed
### Еcли есть SELinux - выкл его
setenforec 0<br/>
sed -i 's/^SELINUX=enforcing$/SELINUX=permissive' /etc/selinux/config
### Вкл доп модули
Идем в /etc/modules-load.d/containerd.conf<br/>
vim /etc/modules-load.d/containerd.conf<br/>
И добавляем в файл два модуля:<br/>
overlay<br/>
br_netfilter<br/>
Подгружаем их:<br/>
modprobe overlay<br/>
modprobe br_netfilter<br/>
### Наводим суету в sysctl:
Идем в /etc/sysctl.conf<br/>
vim /etc/sysctl.conf<br/>
В нем пишем:<br/>
net.ipv4.ip_forward=1<br/>
net.bridge.bridge-nf-call-iptables=1<br/>
net.ipv4.ip_nonlocal_bind=1<br/>
Идем в /etc/sysctl.d/kubernetes.conf<br/>
vim /etc/sysctl.d/kubernetes.conf<br/>
В нем пишем:<br/>
net.bridge.bridge-nf-call-ip6tables=1<br/>
net.bridge.bridge-nf-call-iptables=1<br/>
net.ipv4.ip_forward=1<br/>
Перезапускаем службу:<br/>
sysctl --system<br/>
### Ставим докер и сопутствующие вещицы
apt install apt-transport-https ca-certificates curl software-properties-common<br/>
curl -fsSL https://download.docker.com/linux/ubunt/gpg | sudo apt-key add -<br/>
add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu bionic stable<br/>
apt update<br/>
apt install docker-ce docker-ce-cli containerd.io -y<br/>
containerd config default | tee /etc/containerd/config.toml >/dev/null/ 2>&1<br/>
sed -i 's/SystemdCgroup \=false/SystemdCgroup \true/g' /etc/containerd/config.toml<br/>
systemctl restart containerd<br/>
systemctl enable containerd<br/>
systemctl enable docker<br/>
На этом месте зафиксируем один такой момент - containerd это конечно хорошо, но я в данном мануале поставлю как доп еще cri-dockerd и для работы<br/> 
c кубером буду использовать его по дефолту<br/>
### Ставим cri-dockerd
apt install git wget -y<br/>
git clone https://github.com/Mirantis/cri-dockerd.git<br/>
wget https://go.dev/dl/go1.21.3.linux-amd64.tar.gz<br/>
tar -C /usr/local -xzf go1.21.3.linux-amd64.tar.gz<br/>
export PATH=$PATH:/usr/bin/local/go/bin<br/>
echo 'export PATH=$PATH:/usr/local/go/bin' >>~/.profile<br/>
source ~/.profile<br/>
go version<br/>
cd cri-dockerd/<br/>
mkdir bin<br/>
go build -o bin/cri-dockerd<br/>
mkdip -p /usr/local/bin<br/>
install -o root -g root -m 0755 bin/cri-dockerd /usr/local/bin/cri-dockerd<br/>
cp -a packaging/systemd/* /etc/systemd/system<br/>
sed -i -e 's,/usr/bin/cri-dockerd,/usr/local/bin/cri-dockerd,' /etc/system/cri-docker.service<br/>
systemctl daemon-reload<br/>
systemctl enable --now cri-docker.socket<br/>
### Ставим kubelet, kubeadm, kubectl (kubelet - отвечает за состояниями подов, kubectl - реализует интерфейс командной строки, kubeadm - помогает производить конфигурацию)
curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | apt-key add -<br/>
cat <<EOF | tee /etc/apt/sources.list.d/kubernetes.list<br/>
deb https://apt.kubernetes.io/ kubernetes-xenial main<br/>
EOF<br/>
apt update<br/>
apt install kubelet kubeadm kubectl -y<br/>
systemctl enable kubelet.service<br/>
systemctl enable kubelet<br/>
### После того как эти три пакета поставятся на первую ВМ необходимо проделать все вышеописанное на второй ВМ.<br/>
### На первой ВМ начинаем пыхтеть с kubeadm не забывая указывать cri-socket
kubeadm init --pod-network-cidr=192.168.0.0/16 --cri-socket=unix:///var/run/cri-docker.sock<br/>
--pod-network-cidr=192.168.0.0/16 - пишем так потому что сетевой плагин будет Калико, а не Фланель.
--cri-socket=unix:///var/run/cri-docker.sock - пишем так потому что у нас два CRI и мы выбираем cri-docker.sock, если хочется containerd то<br/>
можно написать --cri-socket=unix:///var/run/containerd.sock<br/>
Все должно на этом моменте начать крутится<br/>
Ну или семь бед один:<br/>
kubeadm reset --cri-socket=unix:///var/run/cri-docker.sock<br/>
Если успех то увидим че нам делать дальше и сообщение с токеном и как типа джойнить ноду - это сообщение обязательно сохранить.<br/>
### Продолжаем настраивать тыры-пыры
mkdir -p $HOME/.kube<br/>
cp -i /etc/kubernetes/admin.conf /root/.kube/config<br/>
chown $(id -u):$(id -g) $HOME/.kube/config<br/>
export KUBECONFIG=/etc/kubernetes/admin.conf<br/>
### Идем на вторую ВМ и джойним ноду
kubeadm join 10.129.0.37:6443 --cri-socket=unix:///var/run/cri-dockerd.sock  --token **тут будет токен**  --discovery-token-ca-cert-hash sha256:**тут<br/> будет хеш**
### Применяем Калико (сетевой плагин)
kubectl apply -f https://docs.projectcalico.org/manifests/calico.yaml
### Ну и как будто бы все, но на самом деле не все
kubectl get nodes<br/>
Заджойненная нода должна быть в списке<br/>
kubectl taint nodes --all node-role.kubernetes.io/control-plane-
kubectl label node **название ноды на котороую хотим роль мастер** node-role.kubernetes.io/master=master
kubectl label node **название ноды на котороую хотим роль воркер** node-role.kubernetes.io/worker=worker